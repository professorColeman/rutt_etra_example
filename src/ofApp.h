#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		ofEasyCam camera;
		ofVideoGrabber webcam;

		ofxPanel gui;
		ofParameter<float> depth;
		vector<ofMesh> lines;
		ofMesh testLine;
		ofImage frame;

		
};
