#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	webcam.setup(800, 600);
	frame.allocate(800, 600, OF_IMAGE_COLOR);

	glLineWidth(6);

	gui.setup();
	gui.add(depth.set("Z-Depth", 20.0));

	camera.setOrientation(glm::vec3(0, 0, 180));
	camera.setPosition(400, 300, 430);

	ofEnableDepthTest();

	for (int j = 0; j < 60;j++) {
		ofMesh line;
		line.setMode(OF_PRIMITIVE_LINES);

		line.addVertex(glm::vec3(0, j*10, 0));
		line.addColor(ofColor(255, 255, 255));

		for (size_t i = 1; i < 80; i++)
		{
			line.addVertex(glm::vec3(i * 10, j*10, 0));
			line.addColor(ofColor(255, 255, 255));
			line.addIndex(i - 1);
			line.addIndex(i);
		}
		lines.push_back(line);
	}
	
}

//--------------------------------------------------------------
void ofApp::update(){
	webcam.update();

	if (webcam.isFrameNew()) {
		frame.setFromPixels(webcam.getPixels());
		int index = 0;
		for (auto&& line: lines) {
			for (size_t i = 0; i < 80; i++)
			{
				ofColor pixel = frame.getColor(i * 10, index*10);
				line.setVertex(i, glm::vec3(i * 10, index*10, pixel.getBrightness()));
				line.setColor(i, pixel);
			}
			index++;
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	camera.begin();
	for (auto&& line : lines) {
		line.drawWireframe();
	}
	camera.end();
}

//--------------------------------------------------------------
